--criação da tabela
CREATE TABLE tb_students(
	cod_student SERIAL PRIMARY KEY,
	MOTHER_EDU INT,
	FATHER_EDU INT,
	GRADE INT,
	SALARY INT,
	PREP_STUDY INT,
	PREP_EXAM INT
);


--2. Escreva um stored procedure que exibe o número de alunos aprovados e cujos pais são ambos PhDs.

DROP PROCEDURE IF EXISTS count_students_approved_phds;
CREATE OR REPLACE PROCEDURE count_students_approved_phds()
LANGUAGE plpgsql
AS $$
DECLARE
    num_approved_phds INT;
BEGIN
    
    SELECT COUNT(*) INTO num_approved_phds
    FROM tb_students
    WHERE MOTHER_EDU = 6
      AND FATHER_EDU = 6
      AND GRADE >= 4;
 
    
    RAISE NOTICE 'Número de alunos aprovados com pais ambos PhDs: %', num_approved_phds;
 
END $$;

--3. Escreva um stored procedure que disponibiliza, utilizando um parâmetro em modo OUT, o número de alunos aprovados dentre aqueles que estudam sozinhos

DROP PROCEDURE IF EXISTS count_students_approved_prep_study;
CREATE OR REPLACE PROCEDURE count_students_approved_prep_study(OUT num_approved_prep_study INT)
LANGUAGE plpgsql
AS $$
BEGIN

    SELECT COUNT(*)
    INTO num_approved_prep_study
    FROM tb_students
    WHERE GRADE >= 4

      AND PREP_STUDY = 1;
 
    RAISE NOTICE 'Número de alunos aprovados que estudam sozinhos: %', num_approved_prep_study;
 
END $$;

--4. Dentre os alunos que têm salário maior que 410, quantos costumam se preparar com frequência (regularmente) para os exames? Escreva uma função que devolva esse número

DROP FUNCTION IF EXISTS count_students_salary_prep_exam();
CREATE OR REPLACE FUNCTION count_students_salary_prep_exam()
RETURNS INT
LANGUAGE plpgsql
AS $$
DECLARE
    num_students INT;
BEGIN

    SELECT COUNT(*)
    INTO num_students
    FROM tb_students
    WHERE SALARY = 5
      AND PREP_EXAM = 2;
 
    RETURN num_students;

END $$;

